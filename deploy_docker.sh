#!/usr/bin/env/ bash

docker-compose down -v --remove-orphans || echo "No containers to bring down..."

docker-compose build

# docker network create onboarding-app || echo "Docker network already exists..."
docker-compose up -d