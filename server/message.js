import { emojiMap } from './chip.js';

export const nobleRecruit = (nobleList) => {
  const returnArray = [];

  nobleList.forEach((obj) => {
    returnArray.push([
      {
        text: `Noble ${
          obj.index
        } | ${obj.noble.getPoint()} | ${obj.noble.getText()} `,
        callback_data: `confirm/12/${obj.index}`,
      },
    ]);
  });
  return returnArray;
};
export const playerChip = (chipPool, args) => {
  const returnArray = [];
  let confirmText = '';
  let callBackData = '';
  args.splice(0, 1);
  Object.entries(chipPool).forEach((chip) => {
    const [key, value] = chip;
    for (let i = 0; i < value; i += 1) {
      const indicator = `${key}${i}`;
      let callBackString = '';
      if (args.includes(indicator)) {
        const copyArgs = args;
        const result = copyArgs.filter((arg) => arg !== indicator);
        confirmText += `${emojiMap(key)} `;
        callBackData += `/${key}`;
        callBackString = `${
          result.length === 0 ? result.join('/') : `/${result.join('/')}`
        }`;
      } else {
        callBackString = `${
          args.length === 0
            ? `/${indicator}`
            : `/${args.join('/')}/${indicator}`
        }`;
      }

      returnArray.push([
        {
          text: `${args.includes(indicator) ? '\u{2705}' : ''}${emojiMap(key)}`,
          callback_data: `9${callBackString}`,
        },
      ]);
    }
  });

  returnArray.push([
    {
      text: `Confirm: ${confirmText}`,
      callback_data: `confirm/9${callBackData}`,
    },
  ]);
  return returnArray;
};
export const handlePurchaseDevelopment = (
  tier1CardList,
  tier2CardList,
  tier3CardList,
) => {
  const returnArray = [];
  tier1CardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier1 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/7/1/${index}`,
      },
    ]);
  });
  tier2CardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier2 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/7/2/${index}`,
      },
    ]);
  });
  tier3CardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier3 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/7/3/${index}`,
      },
    ]);
  });
  returnArray.push([
    {
      text: 'Return to previous menu',
      callback_data: '4',
    },
  ]);

  return returnArray;
};

export const handlePurchaseReserved = (reservedCardList) => {
  const returnArray = [];
  reservedCardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier1 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/8/${index}`,
      },
    ]);
  });
  returnArray.push([
    {
      text: 'Return to previous menu',
      callback_data: '4',
    },
  ]);

  return returnArray;
};

export const handlePurchase = () => [
  [
    {
      text: 'Purchase development card',
      callback_data: '7',
    },
  ],
  [
    {
      text: 'Purchase reserved card',
      callback_data: '8',
    },
  ],
  [
    {
      text: 'Return to main menu',
      callback_data: '5',
    },
  ],
];

export const handleReserveCard = (
  tier1CardList,
  tier2CardList,
  tier3CardList,
) => {
  const returnArray = [];
  tier1CardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier1 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/3/1/${index}`,
      },
    ]);
  });
  tier2CardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier2 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/3/2/${index}`,
      },
    ]);
  });
  tier3CardList.forEach((card, index) => {
    returnArray.push([
      {
        text: `Tier3 | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}`,
        callback_data: `confirm/3/3/${index}`,
      },
    ]);
  });
  returnArray.push([
    {
      text: 'Return to main menu',
      callback_data: '5',
    },
  ]);
  return returnArray;
};

export const handleSingleColor = () => [
  [
    {
      text: '\u{26AA}',
      callback_data: 'confirm/2/white',
    },
  ],
  [
    {
      text: '\u{26AB}',
      callback_data: 'confirm/2/black',
    },
  ],
  [
    {
      text: '\u{1F534}',
      callback_data: 'confirm/2/red',
    },
  ],
  [
    {
      text: '\u{1F7E2}',
      callback_data: 'confirm/2/green',
    },
  ],
  [
    {
      text: '\u{1F535}',
      callback_data: 'confirm/2/blue',
    },
  ],
  [
    {
      text: 'Return to main menu',
      callback_data: '5',
    },
  ],
];

export const handleMultipleColor = (args) => {
  let white = false;
  let black = false;
  let red = false;
  let blue = false;
  let green = false;

  args.forEach((arg) => {
    switch (arg) {
      case 'white':
        white = true;
        break;
      case 'black':
        black = true;
        break;
      case 'red':
        red = true;
        break;
      case 'blue':
        blue = true;
        break;
      case 'green':
        green = true;
        break;
      default:
        break;
    }
  });

  return [
    [
      {
        text: `${white ? '\u{2705}' : ''}\u{26AA}`,
        callback_data: `1${!white ? '/white' : ''}${black ? '/black' : ''}${
          red ? '/red' : ''
        }${blue ? '/blue' : ''}${green ? '/green' : ''}`,
      },
    ],
    [
      {
        text: `${black ? '\u{2705} ' : ''}\u{26AB}`,
        callback_data: `1${white ? '/white' : ''}${!black ? '/black' : ''}${
          red ? '/red' : ''
        }${blue ? '/blue' : ''}${green ? '/green' : ''}`,
      },
    ],
    [
      {
        text: `${red ? '\u{2705} ' : ''}\u{1F534}`,
        callback_data: `1${white ? '/white' : ''}${black ? '/black' : ''}${
          !red ? '/red' : ''
        }${blue ? '/blue' : ''}${green ? '/green' : ''}`,
      },
    ],
    [
      {
        text: `${green ? '\u{2705} ' : ''}\u{1F7E2}`,
        callback_data: `1${white ? '/white' : ''}${black ? '/black' : ''}${
          red ? '/red' : ''
        }${blue ? '/blue' : ''}${!green ? '/green' : ''}`,
      },
    ],
    [
      {
        text: `${blue ? '\u{2705} ' : ''}\u{1F535}`,
        callback_data: `1${white ? '/white' : ''}${black ? '/black' : ''}${
          red ? '/red' : ''
        }${!blue ? '/blue' : ''}${green ? '/green' : ''}`,
      },
    ],
    [
      {
        text: `Confirm: ${blue ? '\u{1F535} ' : ''} ${
          green ? '\u{1F7E2} ' : ''
        }${red ? '\u{1F534} ' : ''}${black ? '\u{26AB} ' : ''}${
          white ? '\u{26AA}' : ''
        }`,
        callback_data: `confirm/1${white ? '/white' : ''}${
          black ? '/black' : ''
        }${red ? '/red' : ''}${blue ? '/blue' : ''}${green ? '/green' : ''}`,
      },
    ],
    [
      {
        text: 'Return to main menu',
        callback_data: '5',
      },
    ],
  ];
};

export const mainMenu = () => [
  [
    {
      text: 'Take 3 gem tokens of different colors.',
      callback_data: 1,
    },
  ],
  [
    {
      text: 'Take 2 gem tokens of the same color.',
      callback_data: 2,
    },
  ],
  [
    {
      text: 'Reserve 1 card and take 1 yellow token',
      callback_data: 3,
    },
  ],
  [{ text: 'Purchase 1 card', callback_data: 4 }],
];
