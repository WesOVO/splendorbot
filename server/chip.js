import { markdownTable } from 'markdown-table';
import stringWidth from 'string-width';

export const emojiMap = (color) => {
  switch (color) {
    case 'red':
      return '\u{1F534}';
    case 'blue':
      return '\u{1F535}';
    case 'green':
      return '\u{1F7E2}';
    case 'black':
      return '\u{26AB}';
    case 'white':
      return '\u{26AA}';
    case 'yellow':
      return '\u{1F7E1}';
    default:
      '';
      break;
  }
};

export const getHand = (chipPool) => {
  let string = '';
  Object.entries(chipPool).forEach((chip) => {
    const [key, value] = chip;
    string += `${value}${emojiMap(key)} `;
  });
  return string;
};

export const getChipText = (header, chipPool) => {
  const tableString = [['Gem', 'Count']];
  const chipText = [];
  Object.entries(chipPool).forEach((chip) => {
    const [key, value] = chip;
    chipText.push([`${key} ${emojiMap(key)}`, value]);
  });
  const combine = [...tableString, ...chipText];
  const chipTable = `${header}\n\n<pre>${markdownTable(combine, {
    delimiterStart: false,
    delimiterEnd: false,
    align: ['r', 'l'],
    padding: false,
    stringLength: stringWidth,
  })}</pre>\n\n`;

  return chipTable;
};
