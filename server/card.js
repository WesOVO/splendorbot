const createCard = (config, _tier) => {
  const { point } = config;
  const { type } = config;
  const { blackCost } = config;
  const { whiteCost } = config;
  const { redCost } = config;
  const { blueCost } = config;
  const { greenCost } = config;
  const tier = _tier;

  return {
    getTier: () => tier,
    getPoint: () => point,
    getTypeEmoji: () => {
      switch (type) {
        case 'red': {
          return '\u{1F534}';
        }
        case 'blue':
          return '\u{1F535}';

        case 'green':
          return '\u{1F7E2}';

        case 'black':
          return '\u{26AB}';

        case 'white':
          return '\u{26AA}';
        case 'yellow':
          return '\u{1F7E1}';

        default:
          return '';
      }
    },
    getType: () => type,
    getTotalCost: () => blackCost + redCost + blueCost + greenCost + whiteCost,
    getBlackCost: () => blackCost,
    getRedCost: () => redCost,
    getBlueCost: () => blueCost,
    getGreenCost: () => greenCost,
    getWhiteCost: () => whiteCost,
    getText: () => {
      let string = '';
      if (redCost !== 0) {
        string += `${redCost}\u{1F534}`;
      }
      if (blackCost !== 0) {
        string += `${blackCost}\u{26AB}`;
      }
      if (whiteCost !== 0) {
        string += `${whiteCost}\u{26AA}`;
      }
      if (blueCost !== 0) {
        string += `${blueCost}\u{1F535}`;
      }
      if (greenCost !== 0) {
        string += `${greenCost}\u{1F7E2}`;
      }
      return string;
    },
  };
};

export default createCard;
