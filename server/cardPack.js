const CardList = require('./cardList');

const Singleton = (() => {
  const CardPack = () => {
    const pack = CardList.createCardList();

    return {
      getPack: () => pack,
    };
  };
  let instance;
  return {
    getInstance: () => {
      if (!instance) {
        instance = CardPack();
      }
      return instance;
    },
  };
})();
module.exports = { Singleton };
