import GameListSingleton from './gameList.js';
import { createGame } from './game.js';
import createGroup from './group.js';
import createCounter from './countDown.js';
import createCardList from './cardList.js';
import db from './database.js';

const gameList = GameListSingleton.getInstance();

const insertUser = (id) => {
  db.query('INSERT into User (id) VALUES (?)', id, (err) => {
    if (err) throw err;
  });
};
const queryDb = async (id) => {
  const query = await db.query(
    'SELECT EXISTS(SELECT 1 FROM User WHERE id = ? LIMIT 1) AS exist',
    id,
  );
  return query[0];
};

export const startGame = async (msg, bot) => {
  const result = await queryDb(msg.from.id);
  if (msg.chat.type === 'group') {
    if (!result[0].exist) {
      bot.sendMessage(
        msg.chat.id,
        'Please start me first before joining game!',
        {
          reply_to_message_id: msg.message_id,
          reply_markup: {
            inline_keyboard: [
              [
                {
                  text: 'Press me to start',
                  url: 'http://t.me/splendorOVO_bot',
                },
              ],
            ],
          },
        },
      );
    } else if (!gameList.getByGroupId(msg.chat.id)) {
      const group = createGroup(msg.chat.id, msg.chat.title);
      const game = createGame(group, bot);
      createCardList(game);
      const counter = createCounter(group, game, bot);
      game.registerCounter(counter);
      gameList.insertGame(msg.chat.id, msg.from.id, game);
      if (game.addPlayer(msg.from.id, msg.from.first_name)) {
        bot.sendMessage(
          msg.chat.id,
          `${
            msg.from.first_name
          } has joined the game! ${game.getPlayerCount()} has joined.`,
        );
      }
    } else {
      const game = gameList.getByPlayerId(msg.from.id);
      if (!game) {
        bot.sendMessage(msg.chat.id, 'Type /join to join!');
        return;
      }
      if (game.getStatus() === 'pending') {
        bot.sendMessage(msg.chat.id, 'You have already joined.');
      }
    }
  } else {
    bot.sendMessage(msg.from.id, 'You have started me!');
    if (!result[0].exist) {
      try {
        insertUser(msg.from.id);
      } catch (err) {
        console.err(err);
      }
    }
  }
};
export const joinGame = async (msg, bot) => {
  const result = await queryDb(msg.from.id);
  if (!result[0].exist) {
    bot.sendMessage(msg.from.id, 'Please start me first before joining game!');
  } else if (msg.chat.type === 'group') {
    const game = gameList.getByGroupId(msg.chat.id);
    if (!game) {
      bot.sendMessage(msg.chat.id, 'Type /start to start!');
    } else if (!gameList.getByPlayerId(msg.from.id)) {
      game.addPlayer(msg.from.id, msg.from.first_name);
      gameList.insertPlayer(msg.from.id, game);
      bot.sendMessage(
        msg.chat.id,
        `${
          msg.from.first_name
        } has joined the game! ${game.getPlayerCount()} has joined.`,
      );
    }
  }
};
export const fleeGame = (msg, bot) => {
  if (msg.chat.type === 'group') {
    const game = gameList.getByGroupId(msg.chat.id);
    if (!game) {
      bot.sendMessage(
        msg.chat.id,
        'No game running in this group. Why not start a new one?',
      );
    } else if (game.getStatus() === 'pending') {
      if (game.removePlayer(msg.from.id)) {
        gameList.removePlayer(msg.from.id, game);
        bot.sendMessage(
          msg.chat.id,
          `${
            msg.from.first_name
          } has left the game! ${game.getPlayerCount()} / 4 has joined.`,
        );
      }
    }
  }
};

export const handleCallBack = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.processCallBack(msg);
  }
};

export const showNobleList = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.showNobleList(msg);
  }
};

export const showPlayerList = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.showPlayerList(msg);
  }
};

export const extendTimer = (msg, bot) => {
  const game = gameList.getByGroupId(msg.chat.id);
  if (!game) {
    bot.sendMessage(
      msg.chat.id,
      'No game running in this group. Why not start a new one?',
    );
  } else {
    game.extendTimer();
  }
};
