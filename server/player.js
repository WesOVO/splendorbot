import { getChipText } from './chip.js';

const createPlayer = (_id, _name) => {
  const id = _id;
  let recordMsg;
  const name = _name;
  const cardList = [];
  const reservedCardList = [];
  const nobleCardList = [];
  let score = 0;
  let chipCount = 0;
  const chipPool = {
    red: 0,
    blue: 0,
    green: 0,
    black: 0,
    white: 0,
    yellow: 0,
  };
  const bonus = {
    red: 0,
    blue: 0,
    green: 0,
    black: 0,
    white: 0,
  };

  return {
    getName: () => name,
    getChipPool: () => chipPool,
    getPlayerId: () => id,
    getScore: () => score,
    getChipCount: () => chipCount,
    getBonus: () => bonus,
    getBonusText: () => getChipText('Bonus table', bonus),
    addCard: (card) => {
      cardList.push(card);
      score += card.getPoint();
      bonus[card.getType()] += 1;
    },
    addReservedCard: (card) => reservedCardList.push(card),
    getReservedCardList: () => reservedCardList,
    addNobleCard: (card) => {
      nobleCardList.push(card[0]);
      score += card[0].getPoint();
    },
    addChip: (chip, count) => {
      chipPool[chip] += count;
      chipCount += count;
    },
    removeChip: (chip, count) => {
      chipPool[chip] -= count;
      chipCount -= count;
    },
    getChipText: () => getChipText('Gem Pool', chipPool),
    setRecordMsg: (msgId) => (recordMsg = msgId),
    getRecordMsg: () => recordMsg,
  };
};

export default createPlayer;
