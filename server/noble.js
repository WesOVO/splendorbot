const createNoble = (config) => {
  const { point } = config;
  const { blackCost } = config;
  const { whiteCost } = config;
  const { redCost } = config;
  const { blueCost } = config;
  const { greenCost } = config;

  return {
    getBlackCost: () => blackCost,
    getRedCost: () => redCost,
    getBlueCost: () => blueCost,
    getGreenCost: () => greenCost,
    getWhiteCost: () => whiteCost,
    getText: () => {
      let string = '';
      if (redCost !== 0) {
        string += `${redCost}\u{1F534}`;
      }
      if (blackCost !== 0) {
        string += `${blackCost}\u{26AB}`;
      }
      if (whiteCost !== 0) {
        string += `${whiteCost}\u{26AA}`;
      }
      if (blueCost !== 0) {
        string += `${blueCost}\u{1F535}`;
      }
      if (greenCost !== 0) {
        string += `${greenCost}\u{1F7E2}`;
      }

      return string;
    },
    getPoint: () => point,
  };
};

export default createNoble;
