const createCounter = (_group, _game, _bot) => {
  const group = _group;
  const game = _game;
  const bot = _bot;
  let time = 120;

  const timer = setInterval(() => {
    time -= 1;
    if (time === 0) {
      clearTimeout(timer);
      timer.unref();
      game.notifyTimesUp();
      return;
    }
    if (time % 30 === 0) {
      bot.sendMessage(group.getGroupId(), `Still have ${time} seconds left`);
    }
  }, 1000);

  return {
    extend: () => {
      time += 30;
    },
    kill: () => {
      clearTimeout(timer);
      timer.unref();
    },
  };
};

export default createCounter;
