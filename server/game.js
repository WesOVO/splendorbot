import { markdownTable } from 'markdown-table';
import stringWidth from 'string-width';
import createPlayer from './player.js';
import GameListSingleton from './gameList.js';
import { wait, shuffleArray } from './util.js';
import { emojiMap, getChipText, getHand } from './chip.js';
import {
  mainMenu,
  handleMultipleColor,
  handleSingleColor,
  handleReserveCard,
  handlePurchase,
  handlePurchaseDevelopment,
  handlePurchaseReserved,
  playerChip,
  nobleRecruit,
} from './message.js';

const gameList = GameListSingleton.getInstance();

export const createGame = (_group, _bot) => {
  const group = _group;
  const playerList = [];
  const bot = _bot;
  let status = 'pending';
  let previousMsg = '';
  let counter;
  let tier1CardList = [];
  let tier2CardList = [];
  let tier3CardList = [];
  let nobleCardList = [];
  const playerMaxCount = 4;
  const chipPool = {
    red: 7,
    blue: 7,
    green: 7,
    black: 7,
    white: 7,
    yellow: 7,
  };
  const tier1 = [];
  const tier2 = [];
  const tier3 = [];
  let playerTurn = 0;

  const notifyPlayerState = async () => {
    let reservedCardText = '';
    const player = playerList[playerTurn];
    if (player.getReservedCardList().length === 0) {
      reservedCardText = 'Empty';
    } else {
      player.getReservedCardList().forEach((card) => {
        reservedCardText += `Tier${card.getTier()} | ${card.getTypeEmoji()} | ${card.getPoint()} | ${card.getText()}\n`;
      });
    }

    bot.deleteMessage(player.getPlayerId(), player.getRecordMsg());
    bot
      .sendMessage(
        player.getPlayerId(),
        `Current score: ${playerList[
          playerTurn
        ].getScore()}\n\nCurrent reserved card:\n${reservedCardText}\n\n${player.getChipText()}\n\n${player.getBonusText()}`,
        {
          parse_mode: 'HTML',
        },
      )
      .then((msg) => {
        player.setRecordMsg(msg.message_id);
      });
  };

  const checkNobleCondition = () => {
    const playerBonus = playerList[playerTurn].getBonus();
    const nobleSatisfy = [];

    nobleCardList.forEach((noble, index) => {
      if (noble.getWhiteCost() > playerBonus.white) return;
      if (noble.getBlackCost() > playerBonus.black) return;
      if (noble.getBlueCost() > playerBonus.blue) return;
      if (noble.getRedCost() > playerBonus.red) return;
      if (noble.getGreenCost() > playerBonus.green) return;

      nobleSatisfy.push({ noble, index });
    });
    if (nobleSatisfy.length === 0) {
      return false;
    }
    if (nobleSatisfy.length === 1) {
      const noble = nobleCardList.splice(nobleSatisfy[0].index, 1);
      playerList[playerTurn].addNobleCard(noble);
      notifyPlayerState();
      bot.sendMessage(
        group.getGroupId(),
        `${playerList[
          (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
            % playerMaxCount
        ].getName()} has recruited a noble`,
        {
          parse_mode: 'HTML',
        },
      );

      bot.sendMessage(
        playerList[
          (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
            % playerMaxCount
        ].getPlayerId(),
        'You recruited a noble',
      );
      return false;
    }

    return nobleSatisfy;
  };

  const getNobleText = () => {
    const tableString = [['Noble', 'P', 'Cost']];
    const nobleText = [];

    nobleCardList.forEach((card, index) => nobleText.push([`${index + 1}`, card.getPoint(), card.getText()]));

    const combine = [...tableString, ...nobleText];
    const nobleTable = `Noble List\n\n<pre>${markdownTable(combine, {
      delimiterStart: false,
      delimiterEnd: false,
      align: ['r', 'c', 'l'],
      padding: false,
      stringLength: stringWidth,
    })}</pre>\n\n`;

    return nobleTable;
  };

  const getPlayerHand = () => {
    let string = 'Player\n\n';

    [0, 1, 2, 3].forEach((index) => {
      string += `${playerList[index].getName()}\nGems:\n${getHand(
        playerList[index].getChipPool(),
      )}\nBonus:\n${getHand(
        playerList[index].getBonus(),
      )}\nPoint:  ${playerList[index].getScore()}\n\n`;
    });
    return string;
  };
  const getCardText = () => {
    const tableString = [['Gem', 'P', 'Cost']];
    const tier1Text = [];
    const tier2Text = [];
    const tier3Text = [];

    [0, 1, 2, 3].forEach((index) => {
      if (index < tier1.length) {
        tier1Text.push([
          `${tier1[index].getTypeEmoji()}`,
          `${tier1[index].getPoint()}`,
          `${tier1[index].getText()}`,
        ]);
      }
      if (index < tier2.length) {
        tier2Text.push([
          `${tier2[index].getTypeEmoji()}`,
          `${tier2[index].getPoint()}`,
          `${tier2[index].getText()}`,
        ]);
      }
      if (index < tier3.length) {
        tier3Text.push([
          `${tier3[index].getTypeEmoji()}`,
          `${tier3[index].getPoint()}`,
          `${tier3[index].getText()}`,
        ]);
      }
    });

    const combine1 = [...tableString, ...tier1Text];
    const combine2 = [...tableString, ...tier2Text];
    const combine3 = [...tableString, ...tier3Text];
    const markdown1 = `Tier 1\n\n<pre>${markdownTable(combine1, {
      delimiterStart: false,
      delimiterEnd: false,
      align: ['r', 'c', 'l'],
      padding: false,
      stringLength: stringWidth,
    })}</pre>\n\n`;
    const markdown2 = `Tier 2\n\n<pre>${markdownTable(combine2, {
      delimiterStart: false,
      delimiterEnd: false,
      align: ['r', 'c', 'l'],
      padding: false,
      stringLength: stringWidth,
    })}</pre>\n\n`;
    const markdown3 = `Tier 3\n\n<pre>${markdownTable(combine3, {
      delimiterStart: false,
      delimiterEnd: false,
      align: ['r', 'c', 'l'],
      padding: false,
      stringLength: stringWidth,
    })}</pre>\n\n`;

    return markdown1 + markdown2 + markdown3;
  };

  const ChipExceed = () => {
    bot.sendMessage(
      group.getGroupId(),
      `${playerList[
        playerTurn
      ].getName()} has exceeded his maximum gems count on his hand. Let's wait him to return some of the gems.`,
      {
        parse_mode: 'HTML',
      },
    );

    bot.sendMessage(
      playerList[playerTurn].getPlayerId(),
      `You have exceeded your maximum gems count (10). Please return ${
        playerList[playerTurn].getChipCount() - 10
      } gem(s)`,
      {
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: playerChip(playerList[playerTurn].getChipPool(), [
            '9',
          ]),
        },
      },
    );
  };
  const notifyNextPlayer = () => {
    bot.sendMessage(
      playerList[playerTurn].getPlayerId(),
      `Your turn.\n\n${getGameStateMessage()}`,
      {
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: mainMenu(),
        },
      },
    );
  };

  const getGameStateMessage = () => {
    const chipText = getChipText('Gem Pool', chipPool);
    const cardText = getCardText();

    return chipText + cardText;
  };

  const checkChipEnough = (item, count) => {
    if (count === 1) {
      return chipPool[item] >= count;
    }
    return chipPool[item] >= 4;
  };

  const removeChip = (item, count) => {
    chipPool[item] -= count;
  };
  const addChip = (item, count) => {
    chipPool[item] += count;
  };

  const validatePurchase = (card, player) => {
    const chipPool = player.getChipPool();
    const bonus = player.getBonus();
    let yellowUsed = 0;

    if (card.getWhiteCost() > chipPool.white + bonus.white) {
      if (
        chipPool.yellow - yellowUsed
        && card.getWhiteCost() - chipPool.white - bonus.white
          <= chipPool.yellow - yellowUsed
      ) {
        yellowUsed += card.getWhiteCost() - chipPool.white - bonus.white;
      } else {
        return false;
      }
    }

    if (card.getBlackCost() > chipPool.black + bonus.black) {
      if (
        chipPool.yellow - yellowUsed
        && card.getBlackCost() - chipPool.black - bonus.black
          <= chipPool.yellow - yellowUsed
      ) {
        yellowUsed += card.getBlackCost() - chipPool.black - bonus.black;
      } else {
        return false;
      }
    }

    if (card.getBlueCost() > chipPool.blue + bonus.blue) {
      if (
        chipPool.yellow - yellowUsed
        && card.getBlueCost() - chipPool.blue - bonus.blue
          <= chipPool.yellow - yellowUsed
      ) {
        yellowUsed += card.getBlueCost() - chipPool.blue - bonus.blue;
      } else {
        return false;
      }
    }

    if (card.getRedCost() > chipPool.red + bonus.red) {
      if (
        chipPool.yellow - yellowUsed
        && card.getRedCost() - chipPool.red - bonus.red
          <= chipPool.yellow - yellowUsed
      ) {
        yellowUsed += card.getRedCost() - chipPool.red - bonus.red;
      } else {
        return false;
      }
    }

    if (card.getGreenCost() > chipPool.green + bonus.green) {
      if (
        chipPool.yellow - yellowUsed
        && card.getGreenCost() - chipPool.green - bonus.green
          <= chipPool.yellow - yellowUsed
      ) {
        yellowUsed += card.getGreenCost() - chipPool.green - bonus.green;
      } else {
        return false;
      }
    }
    return true;
  };

  const purchase = (card, player) => {
    const chipPool = player.getChipPool();
    const bonus = player.getBonus();

    if (card.getWhiteCost() !== 0 && bonus.white !== card.getWhiteCost()) {
      for (let i = 0; i < card.getWhiteCost() - bonus.white; i += 1) {
        if (chipPool.white > 0) {
          player.removeChip('white', 1);
          addChip('white', 1);
        } else {
          player.removeChip('yellow', 1);
          addChip('yellow', 1);
        }
      }
    }
    if (card.getBlackCost() !== 0 && bonus.black !== card.getBlackCost()) {
      for (let i = 0; i < card.getBlackCost() - bonus.black; i += 1) {
        if (chipPool.black > 0) {
          player.removeChip('black', 1);
          addChip('black', 1);
        } else {
          player.removeChip('yellow', 1);
          addChip('yellow', 1);
        }
      }
    }
    if (card.getRedCost() !== 0 && bonus.red !== card.getRedCost()) {
      for (let i = 0; i < card.getRedCost() - bonus.red; i += 1) {
        if (chipPool.red > 0) {
          player.removeChip('red', 1);
          addChip('red', 1);
        } else {
          player.removeChip('yellow', 1);
          addChip('yellow', 1);
        }
      }
    }
    if (card.getGreenCost() !== 0 && bonus.green !== card.getGreenCost()) {
      for (let i = 0; i < card.getGreenCost() - bonus.green; i += 1) {
        if (chipPool.green > 0) {
          player.removeChip('green', 1);
          addChip('green', 1);
        } else {
          player.removeChip('yellow', 1);
          addChip('yellow', 1);
        }
      }
    }

    if (card.getBlueCost() !== 0 && bonus.green !== card.getBlueCost()) {
      for (let i = 0; i < card.getBlueCost() - bonus.blue; i += 1) {
        if (chipPool.blue > 0) {
          player.removeChip('blue', 1);
          addChip('blue', 1);
        } else {
          player.removeChip('yellow', 1);
          addChip('yellow', 1);
        }
      }
    }
  };
  const checkWin = () => {
    if (
      playerList[
        (((playerTurn - 1) % playerMaxCount) + playerMaxCount) % playerMaxCount
      ].getScore() >= 15
    ) {
      status = 'end';
      return true;
    }
    return false;
  };

  const cardRefill = (tier, index) => {
    let reservedCard;
    switch (tier) {
      case '1':
        reservedCard = tier1.splice(index, 1);
        if (tier1CardList.length !== 0) {
          tier1.splice(index, 0, tier1CardList.pop());
        }
        break;
      case '2':
        reservedCard = tier2.splice(index, 1);
        if (tier2CardList.length !== 0) {
          tier2.splice(index, 0, tier2CardList.pop());
        }
        break;
      case '3':
        reservedCard = tier3.splice(index, 1);
        if (tier3CardList.length !== 0) {
          tier3.splice(index, 0, tier3CardList.pop());
        }
        break;
      default:
        break;
    }
    return reservedCard;
  };

  const handleResponse = async (msg) => {
    const path = msg.data.split('/');
    if (path[0] === 'confirm') {
      switch (path[1]) {
        case '1':
          path.splice(0, 2);
          if (path.length !== 3) {
            bot.answerCallbackQuery(msg.id, {
              text: 'You have to choose 3 colors.',
              show_alert: true,
            });
          } else {
            let actionText1 = '';
            let errorFlag1 = false;
            path.forEach((item) => {
              if (!checkChipEnough(item, 1)) {
                bot.answerCallbackQuery(msg.id, {
                  text: `${item} is not enough`,
                  show_alert: true,
                });
                errorFlag1 = true;
              }
            });
            if (!errorFlag1) {
              path.forEach((item) => {
                removeChip(item, 1);
                playerList[playerTurn].addChip(item, 1);
                actionText1 += emojiMap(item);
              });
              await wait(1000);
              notifyPlayerState();
              bot.sendMessage(
                playerList[playerTurn].getPlayerId(),
                `You have taken ${actionText1}`,
              );
              bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
              if (playerList[playerTurn].getChipCount() > 10) {
                bot.sendMessage(
                  group.getGroupId(),
                  `${playerList[
                    (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                      % playerMaxCount
                  ].getName()} has taken ${actionText1} from the pool!\n\n`,
                  {
                    parse_mode: 'HTML',
                  },
                );
                ChipExceed();
              } else {
                playerTurn = (((playerTurn + 1) % playerMaxCount) + playerMaxCount)
                  % playerMaxCount;
                broadCast(
                  `${playerList[
                    (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                      % playerMaxCount
                  ].getName()} has taken ${actionText1} from the pool!\n\n`,
                );
              }
            }
          }
          break;

        case '2':
          path.splice(0, 2);
          let errorFlag2 = false;
          if (!checkChipEnough(path[0], 2)) {
            bot.answerCallbackQuery(msg.id, {
              text: `${path[0]} is not enough`,
              show_alert: true,
            });
            errorFlag2 = true;
          }
          if (!errorFlag2) {
            let actionText2 = '';
            removeChip(path[0], 2);
            playerList[playerTurn].addChip(path[0], 2);
            actionText2 += emojiMap(path[0]);
            await wait(1000);
            notifyPlayerState();
            bot.sendMessage(
              playerList[playerTurn].getPlayerId(),
              `You have taken 2 ${actionText2}`,
            );
            bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
            if (playerList[playerTurn].getChipCount() > 10) {
              ChipExceed();
            } else {
              playerTurn = (((playerTurn + 1) % playerMaxCount) + playerMaxCount)
                % playerMaxCount;
              broadCast(
                `${playerList[
                  (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                    % playerMaxCount
                ].getName()} has taken 2 ${actionText2} from the pool!\n\n`,
              );
            }
          }
          break;
        case '3':
          let reservedCard;
          let actionText3 = '';
          path.splice(0, 2);
          reservedCard = cardRefill(path[0], path[1]);
          let yellowFlag = true;
          if (chipPool.yellow !== 0) {
            yellowFlag = false;
            removeChip('yellow', 1);
            playerList[playerTurn].addChip('yellow', 1);
          }
          playerList[playerTurn].addReservedCard(reservedCard[0]);
          actionText3 += `Tier ${
            path[0]
          } | ${reservedCard[0].getTypeEmoji()} | ${reservedCard[0].getPoint()} | ${reservedCard[0].getText()}`;
          await wait(1000);
          notifyPlayerState();
          bot.sendMessage(
            playerList[playerTurn].getPlayerId(),
            `You have reserved the following card:\n${actionText3} ${
              !yellowFlag ? `and taken 1 ${emojiMap('yellow')}` : ''
            }`,
          );
          bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
          playerTurn = (((playerTurn + 1) % playerMaxCount) + playerMaxCount)
            % playerMaxCount;
          broadCast(
            `${playerList[
              (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                % playerMaxCount
            ].getName()} has reserved the following card:\n${actionText3} ${
              !yellowFlag ? `and taken 1 ${emojiMap('yellow')}` : ''
            }`,
          );
          break;
        case '7':
          path.splice(0, 2);
          let errorFlag7 = false;
          let cardToBuy;
          switch (path[0]) {
            case '1':
              cardToBuy = tier1[path[1]];
              break;
            case '2':
              cardToBuy = tier2[path[1]];
              break;
            case '3':
              cardToBuy = tier3[path[1]];
              break;
            default:
              break;
          }

          if (!validatePurchase(cardToBuy, playerList[playerTurn])) {
            bot.answerCallbackQuery(msg.id, {
              text: 'You dont have enough gems.',
              show_alert: true,
            });

            errorFlag7 = true;
          }
          if (!errorFlag7) {
            purchase(cardToBuy, playerList[playerTurn]);
            playerList[playerTurn].addCard(cardToBuy);
            cardRefill(path[0], path[1]);
            await wait(1000);
            notifyPlayerState();
            bot.sendMessage(
              playerList[playerTurn].getPlayerId(),
              `You have bought\nTier ${cardToBuy.getTier()} | ${cardToBuy.getTypeEmoji()} | ${cardToBuy.getPoint()} | ${cardToBuy.getText()}`,
            );

            bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
            const nobleCheck7 = checkNobleCondition();
            if (nobleCheck7) {
              bot.sendMessage(
                group.getGroupId(),
                `${playerList[
                  playerTurn
                ].getName()} has satisfied to choose a noble.`,
                {
                  parse_mode: 'HTML',
                },
              );

              bot.sendMessage(
                playerList[playerTurn].getPlayerId(),
                'You can now recruit a noble',
                {
                  parse_mode: 'HTML',
                  reply_markup: {
                    inline_keyboard: nobleRecruit(nobleCheck7),
                  },
                },
              );
            } else {
              playerTurn = (((playerTurn + 1) % playerMaxCount) + playerMaxCount)
                % playerMaxCount;
              broadCast(
                `${playerList[
                  (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                    % playerMaxCount
                ].getName()} has bought\nTier ${cardToBuy.getTier()} | ${cardToBuy.getTypeEmoji()} | ${cardToBuy.getPoint()} | ${cardToBuy.getText()}\n\n`,
              );
            }
          }
          break;
        case '8':
          path.splice(0, 2);
          let errorFlag8 = false;

          const reservedCardToBuy = playerList[
            playerTurn
          ].getReservedCardList()[path[0]];

          if (!validatePurchase(reservedCardToBuy, playerList[playerTurn])) {
            bot.answerCallbackQuery(msg.id, {
              text: 'You dont have enough gems.',
              show_alert: true,
            });
            errorFlag8 = true;
          }

          if (!errorFlag8) {
            purchase(reservedCardToBuy, playerList[playerTurn]);
            playerList[playerTurn].addCard(
              reservedCardToBuy,
              reservedCardToBuy.getPoint(),
            );
            await wait(1000);
            notifyPlayerState();
            bot.sendMessage(
              playerList[playerTurn].getPlayerId(),
              `You have bought\nTier ${reservedCardToBuy.getTier()} | ${reservedCardToBuy.getTypeEmoji()} | ${reservedCardToBuy.getPoint()} | ${reservedCardToBuy.getText()}`,
            );
            bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
            const nobleCheck8 = checkNobleCondition();
            if (nobleCheck8) {
              bot.sendMessage(
                group.getGroupId(),
                `${playerList[
                  playerTurn
                ].getName()} has satisfied to choose a noble.`,
                {
                  parse_mode: 'HTML',
                },
              );

              bot.sendMessage(
                playerList[playerTurn].getPlayerId(),
                'You can now recruit a noble',
                {
                  parse_mode: 'HTML',
                  reply_markup: {
                    inline_keyboard: nobleRecruit(nobleCheck8),
                  },
                },
              );
            } else {
              playerTurn = (((playerTurn + 1) % playerMaxCount) + playerMaxCount)
                % playerMaxCount;
              broadCast(
                `${playerList[
                  (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                    % playerMaxCount
                ].getName()} has bought\nTier ${reservedCardToBuy.getTier()} | ${reservedCardToBuy.getTypeEmoji()} | ${reservedCardToBuy.getPoint()} | ${reservedCardToBuy.getText()}\n\n`,
              );
            }
          }
          break;

        case '9':
          let errorFlag9 = false;
          let actionText9 = '';
          path.splice(0, 2);
          if (path.length !== playerList[playerTurn].getChipCount() - 10) {
            bot.answerCallbackQuery(msg.id, {
              text: 'Please return enough gems.',
              show_alert: true,
            });
            errorFlag9 = true;
          }
          if (!errorFlag9) {
            path.forEach((item) => {
              addChip(item, 1);
              playerList[playerTurn].removeChip(item, 1);
              actionText9 += emojiMap(item);
            });
            await wait(1000);
            notifyPlayerState();
            bot.sendMessage(
              playerList[playerTurn].getPlayerId(),
              `You have returned ${actionText9} to the pool.`,
            );
            bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
            playerTurn = (((playerTurn + 1) % playerMaxCount) + playerMaxCount)
              % playerMaxCount;
            broadCast(
              `${playerList[
                (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                  % playerMaxCount
              ].getName()} has returned ${actionText9} to the pool!\n\n`,
            );
          }
          break;

        case '12':
          path.splice(0, 2);
          const noble = nobleCardList.splice(path[0], 1);
          playerList[playerTurn].addNobleCard(noble);
          notifyPlayerState();
          bot.deleteMessage(msg.message.chat.id, msg.message.message_id);
          bot.sendMessage(
            group.getGroupId(),
            `${playerList[playerTurn].getName()} has recruited a noble`,
            {
              parse_mode: 'HTML',
            },
          );

          bot.sendMessage(
            playerList[
              (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
                % playerMaxCount
            ].getPlayerId(),
            'You recruited a noble',
          );
          broadCast();
          break;
        default:
          break;
      }
    } else {
      switch (path[0]) {
        case '1':
          bot.editMessageReplyMarkup(
            { inline_keyboard: handleMultipleColor(path) },
            {
              chat_id: msg.message.chat.id,
              message_id: msg.message.message_id,
              parse_mode: 'HTML',
            },
          );

          break;
        case '2':
          bot.editMessageReplyMarkup(
            { inline_keyboard: handleSingleColor() },
            {
              chat_id: msg.message.chat.id,
              message_id: msg.message.message_id,
              parse_mode: 'HTML',
            },
          );
          break;
        case '3':
          if (playerList[playerTurn].getReservedCardList().length === 3) {
            bot.answerCallbackQuery(msg.id, {
              text: 'You cannot reserve more than 3 cards',
              show_alert: true,
            });
          } else {
            if (chipPool.yellow === 0) {
              bot.editMessageText(
                'Attention! No more yellow gems in pool, but you can still reserve a card.',
                {
                  chat_id: msg.message.chat.id,
                  message_id: msg.message.message_id,
                  parse_mode: 'HTML',
                },
              );
            }
            bot.editMessageReplyMarkup(
              {
                inline_keyboard: handleReserveCard(tier1, tier2, tier3),
              },
              {
                chat_id: msg.message.chat.id,
                message_id: msg.message.message_id,
                parse_mode: 'HTML',
              },
            );
          }
          break;
        case '4':
          bot.editMessageReplyMarkup(
            { inline_keyboard: handlePurchase() },
            {
              chat_id: msg.message.chat.id,
              message_id: msg.message.message_id,
              parse_mode: 'HTML',
            },
          );
          break;
        case '5':
          bot.editMessageReplyMarkup(
            { inline_keyboard: mainMenu() },
            {
              chat_id: msg.message.chat.id,
              message_id: msg.message.message_id,
              parse_mode: 'HTML',
            },
          );
          break;
        case '7':
          bot.editMessageReplyMarkup(
            { inline_keyboard: handlePurchaseDevelopment(tier1, tier2, tier3) },
            {
              chat_id: msg.message.chat.id,
              message_id: msg.message.message_id,
              parse_mode: 'HTML',
            },
          );

          break;
        case '8':
          if (playerList[playerTurn].getReservedCardList().length !== 0) {
            bot.editMessageReplyMarkup(
              {
                inline_keyboard: handlePurchaseReserved(
                  playerList[playerTurn].getReservedCardList(),
                ),
              },
              {
                chat_id: msg.message.chat.id,
                message_id: msg.message.message_id,
                parse_mode: 'HTML',
              },
            );
          } else {
            bot.answerCallbackQuery(msg.id, {
              text: 'You dont have any reserved cards.',
              show_alert: true,
            });
          }
          break;
        case '9':
          bot.editMessageReplyMarkup(
            {
              inline_keyboard: playerChip(
                playerList[playerTurn].getChipPool(),
                path,
              ),
            },
            {
              chat_id: msg.message.chat.id,
              message_id: msg.message.message_id,
              parse_mode: 'HTML',
            },
          );
          break;

        default:
          break;
      }
    }
  };

  const broadCast = async (playAction) => {
    if (checkWin()) {
      bot.sendMessage(
        group.getGroupId(),
        `${playerList[
          (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
            % playerMaxCount
        ].getName()} has won the game with ${playerList[
          (((playerTurn - 1) % playerMaxCount) + playerMaxCount)
            % playerMaxCount
        ].getScore()} points`,
        {
          parse_mode: 'HTML',
        },
      );

      terminate();
    } else {
      const endText = `Now is ${playerList[playerTurn].getName()}'s turn`;
      if (playAction !== undefined) {
        await bot.sendMessage(group.getGroupId(), playAction, {
          parse_mode: 'HTML',
        });
      }
      if (previousMsg !== '') {
        bot.deleteMessage(group.getGroupId(), previousMsg);
      }
      bot
        .sendMessage(group.getGroupId(), `${getGameStateMessage()}${endText}`, {
          parse_mode: 'HTML',
        })
        .then((msg) => (previousMsg = msg.message_id));

      notifyNextPlayer();
    }
  };

  const init = async () => {
    if (
      !(
        tier1CardList.length
        && tier2CardList.length
        && tier3CardList.length
        && nobleCardList.length
      )
    ) {
      bot.sendMessage(group.getGroupId(), 'Game environment is setting up.');
      setTimeout(
        () => bot.sendMessage(group.getGroupId(), "Let's get started"),
        5000,
      );
      await wait(5000);
    }
    status = 'running';
    if (counter) {
      counter.kill();
    }

    [1, 2, 3, 4].forEach(() => {
      tier1.push(tier1CardList.pop());
      tier2.push(tier2CardList.pop());
      tier3.push(tier3CardList.pop());
    });

    shuffleArray(playerList);
    playerList.forEach((player) => {
      bot
        .sendMessage(
          player.getPlayerId(),
          `Current score: ${player.getScore()}\n\nReserved Card:\nEmpty\n\n${player.getChipText()}\n\n${player.getBonusText()}`,
          {
            parse_mode: 'HTML',
          },
        )
        .then((msg) => {
          player.setRecordMsg(msg.message_id);
        });
    });
    await wait(1000);
    bot.sendMessage(group.getGroupId(), getNobleText(), {
      parse_mode: 'HTML',
    });
    broadCast();
  };

  const terminate = () => {
    gameList.removeGame(group.getGroupId(), playerList);
    if (counter) {
      counter.kill();
    }
  };

  return {
    addPlayer: (playerId, playerName) => {
      playerList.push(createPlayer(playerId, playerName));
      bot.sendMessage(playerId, 'You have joined');
      if (playerList.length === playerMaxCount) {
        init();
      }
      return true;
    },
    getPlayerCount: () => playerList.length,
    getPlayerId: () => playerId,
    removePlayer: (playerId) => {
      let found;
      playerList.forEach((player, index) => {
        if (player.getPlayerId() === playerId) {
          found = index;
        }
      });
      playerList.splice(index, 1);
      bot.sendMessage(playerId, 'You have left the game');
      if (playerList.length === 0) {
        terminate();
        bot.sendMessage(group.getGroupId(), 'Game Terminated.');
        return false;
      }
      return true;
    },
    getStatus: () => status,
    registerCounter: (aCounter) => {
      counter = aCounter;
    },
    notifyTimesUp: () => {
      bot.sendMessage(
        group.getGroupId(),
        'Players are not enough to start a game. Game Terminated.',
      );
      terminate();
    },

    updateCardList: (list) => {
      if (Object.keys(list).length === 1) {
        nobleCardList = list[Object.keys(list)[0]];
      } else {
        tier1CardList = list.tier1CardList;
        tier2CardList = list.tier2CardList;
        tier3CardList = list.tier3CardList;
      }
    },
    processCallBack: (msg) => {
      handleResponse(msg);
    },
    showNobleList: (msg) => {
      bot.sendMessage(msg.from.id, getNobleText(), {
        parse_mode: 'HTML',
      });
    },
    showPlayerList: (msg) => {
      bot.sendMessage(msg.from.id, getPlayerHand(), {
        parse_mode: 'HTML',
      });
    },
    extendTimer: () => counter.extend(),
  };
};
