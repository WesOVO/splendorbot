import fs from 'fs';
import createNoble from './noble.js';
import createCard from './card.js';
import { shuffleArray } from './util.js';

const createCardList = (_game) => {
  const game = _game;
  const tier1CardList = [];
  const tier2CardList = [];
  const tier3CardList = [];
  const nobleCardList = [];

  fs.readFile('./assets/card.json', (err, data) => {
    if (err) throw err;
    const cardConfig = JSON.parse(data);

    Object.entries(cardConfig).forEach((entry) => {
      const [key, value] = entry;
      value.forEach((config) => {
        if (key === '1') {
          tier1CardList.push(createCard(config, 1));
        }
        if (key === '2') {
          tier2CardList.push(createCard(config, 2));
        }
        if (key === '3') {
          tier3CardList.push(createCard(config, 3));
        }
      });
    });
    shuffleArray(tier1CardList);
    shuffleArray(tier2CardList);
    shuffleArray(tier3CardList);

    game.updateCardList({ tier1CardList, tier2CardList, tier3CardList });
  });
  fs.readFile('./assets/nobleCard.json', (err, data) => {
    if (err) throw err;
    const cardConfig = JSON.parse(data);

    Object.entries(cardConfig).forEach((entry) => {
      const [key, value] = entry;
      nobleCardList.push(createNoble(value));
    });
    shuffleArray(nobleCardList);
    nobleCardList.splice(0, 3);
    game.updateCardList({ nobleCardList });
  });
};

export default createCardList;
