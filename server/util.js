const wait = (ms) => new Promise((resolve) => setTimeout(() => resolve(), ms));

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
};

export { wait, shuffleArray };
