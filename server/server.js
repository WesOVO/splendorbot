import dotenv from 'dotenv';
import TelegramBot from 'node-telegram-bot-api';

import {
  startGame,
  fleeGame,
  joinGame,
  handleCallBack,
  showNobleList,
  showPlayerList,
  extendTimer,
} from './Command.js';

dotenv.config();

const url = process.env.NGROK_URL;
const TOKEN = process.env.BOT_TOKEN;

const bot = new TelegramBot(TOKEN, { polling: true });

bot.setWebHook(`${url}/bot${TOKEN}`);

bot.onText(/\/start/, (msg) => {
  startGame(msg, bot);
});

bot.onText(/\/flee/, (msg) => {
  fleeGame(msg, bot);
});

bot.onText(/\/join/, (msg) => {
  joinGame(msg, bot);
});

bot.onText(/\/noblelist/, (msg) => {
  showNobleList(msg, bot);
});

bot.onText(/\/playerlist/, (msg) => {
  showPlayerList(msg, bot);
});

bot.onText(/\/extend/, (msg) => {
  extendTimer(msg, bot);
});

bot.on('callback_query', (msg) => {
  handleCallBack(msg);
});

bot.on('new_chat_members', (msg) => {
  if (msg.new_chat_participant.id === process.env.BOT_ID) {
    bot.sendMessage(msg.chat.id, 'Hi guys. I am Splendor Bot');
    bot.sendMessage(
      process.env.ADMIN_ID,
      `New group invitation:\nGroup name:${msg.chat.title}\nOperator:${msg.from.first_name}TgId:${msg.from.username}`,
    );
  }
});
bot.on('polling_error', console.log);
