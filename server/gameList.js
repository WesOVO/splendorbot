const Singleton = (() => {
  const gameList = () => {
    const listByGroup = new Map();
    const listByPlayer = new Map();
    return {
      getByGroupId: (groupId) => listByGroup.get(groupId),
      getByPlayerId: (playerId) => listByPlayer.get(playerId),
      insertGame: (groupId, playerId, game) => {
        listByGroup.set(groupId, game);
        listByPlayer.set(playerId, game);
      },
      insertPlayer: (playerId, game) => {
        listByPlayer.set(playerId, game);
      },
      removePlayer: (playerId) => listByPlayer.delete(playerId),
      removeGame: (groupId, playerList) => {
        listByGroup.delete(groupId);
        if (!playerList.length) { playerList.forEach((player) => listByPlayer.delete(player.getPlayerId())); }
      },
    };
  };
  let instance;
  return {
    getInstance: () => {
      if (!instance) {
        instance = gameList();
      }
      return instance;
    },
  };
})();

export default Singleton;
